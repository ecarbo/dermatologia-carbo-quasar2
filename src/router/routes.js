
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: 'curriculum', component: () => import('pages/Curriculum.vue') },
      { path: 'contacto', component: () => import('pages/Contacto.vue') },
      { path: 'info-piel', component: () => import('pages/InfoPiel.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
